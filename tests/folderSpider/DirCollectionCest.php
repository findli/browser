<?php

require 'init_autoloader.php';

class DirCollectionCest
{
    public $folderSpiderService;
    public function _before(FolderSpiderTester $I)
    {
        $this->folderSpiderService = new \App\Service\FolderSpider\FolderSpiderService();
    }

    public function _after(FolderSpiderTester $I)
    {
    }

    public function dirCollecton(FolderSpiderTester $I)
    {
        
        $I->assertTrue($this->folderSpiderService->run());
        echo get_class($I);
    }
}
